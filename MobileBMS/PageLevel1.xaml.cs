﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileBMS
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageLevel1 : ContentPage
    {
        App currApp;
        public PageLevel1()
        {
            InitializeComponent();
            currApp = (App)Application.Current;
            StartAsyncTask();
        }
        public async Task StartAsyncTask()
        {
            while (true)
            {
                Application.Current.Dispatcher.BeginInvokeOnMainThread(() => 
                {
                    entryPowerUsage.Text = currApp.slmpData.floorsPowerUsage[1].ToString("0.000") + " kW";
                    buttonRoom11.Text = "Pokój 11\n" + currApp.slmpData.currentTemp[6].ToString("0.0") + "°C";
                    buttonRoom12.Text = "Pokój 12\n" + currApp.slmpData.currentTemp[7].ToString("0.0") + "°C";
                    buttonRoom13.Text = "Pokój 13\n" + currApp.slmpData.currentTemp[8].ToString("0.0") + "°C";
                    buttonRoom14.Text = "Pokój 14\n" + currApp.slmpData.currentTemp[9].ToString("0.0") + "°C";
                });
                await Task.Delay(200);
            }
        }
        private async void buttonRoom11_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(6, "11"));
        }
        private async void buttonRoom12_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(7, "12"));
        }
        private async void buttonRoom13_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(8, "13"));
        }
        private async void buttonRoom14_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(9, "14"));
        }
    }
}