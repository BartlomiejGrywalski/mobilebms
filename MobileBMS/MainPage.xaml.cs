﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace MobileBMS
{
    public partial class MainPage : ContentPage
    {
        App currApp;
        public MainPage()
        {
            InitializeComponent();
            currApp = (App)Application.Current;
            StartAsyncTask();
        }
        public async Task StartAsyncTask()
        {
            while (true)
            {
                await Task.Delay(200);
                Application.Current.Dispatcher.BeginInvokeOnMainThread(() => { 
                    labelShowAllData.Text = currApp.GetMessage();
                    entryTotalPowerUsage.Text = currApp.slmpData.totalPowerUsage.ToString("0.000") + " kW";
                });
            }
        }
        private async void NaviToLevel0_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PageLevel0());
        }
        private async void NaviToLevel1_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PageLevel1());
        }
        private async void NaviToLevel2_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PageLevel2());
        }
    }
}