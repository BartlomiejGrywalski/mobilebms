﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileBMS
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageLevel0 : ContentPage
    {
        App currApp;
        public PageLevel0()
        {
            InitializeComponent();
            currApp = (App)Application.Current;
            StartAsyncTask();
        }
        public async Task StartAsyncTask()
        {
            while (true)
            {
                Application.Current.Dispatcher.BeginInvokeOnMainThread(() => 
                {
                    entryPowerUsage.Text = currApp.slmpData.floorsPowerUsage[0].ToString("0.000") + " kW";
                    buttonRoom01.Text = "Pokój 01\n" + currApp.slmpData.currentTemp[0].ToString("0.0") + "°C";
                    buttonRoom02.Text = "Pokój 02\n" + currApp.slmpData.currentTemp[1].ToString("0.0") + "°C";
                    buttonRoom03.Text = "Pokój 03\n" + currApp.slmpData.currentTemp[2].ToString("0.0") + "°C";
                    buttonRoom04.Text = "Pokój 04\n" + currApp.slmpData.currentTemp[3].ToString("0.0") + "°C";
                    buttonRoom05.Text = "Pokój 05\n" + currApp.slmpData.currentTemp[4].ToString("0.0") + "°C";
                    buttonRoom06.Text = "Pokój 06\n" + currApp.slmpData.currentTemp[5].ToString("0.0") + "°C";
                });
                await Task.Delay(200);
            }
        }
        private async void buttonRoom01_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(0, "01"));
        }
        private async void buttonRoom02_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(1, "02"));
        }
        private async void buttonRoom03_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(2, "03"));
        }
        private async void buttonRoom04_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(3, "04"));
        }
        private async void buttonRoom05_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(4, "05"));
        }
        private async void buttonRoom06_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RoomCommon(5, "06"));
        }
    }
}